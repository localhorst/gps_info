use nmea_parser::*;
use std::io::BufRead;
use std::io::BufReader;
use std::str;

fn main() {
    println!("gpsInfo!");
    loop {
        for line in BufReader::new(
            serialport::new("/dev/ttyUSB0", 38400)
                .open()
                .expect("Failed to open port"),
        )
        .lines()
        {
            match line {
                Ok(v) => filter_coordinates(&v),
                Err(_e) => (), // println!("error parsing line: {:?}", e),
            }
        }
    }
}

fn filter_coordinates(nmea: &str) {
    if nmea.starts_with("$") {
        // println!("line: {}", nmea);

        // Create parser and define sample sentences
        let mut parser = NmeaParser::new();

        match parser.parse_sentence(nmea).unwrap() {
            ParsedMessage::VesselDynamicData(vdd) => {
                println!("MMSI:    {}", vdd.mmsi);
                println!("Speed:   {:.1} kts", vdd.sog_knots.unwrap());
                println!("Heading: {}°", vdd.heading_true.unwrap());
                println!("");
            }
            ParsedMessage::VesselStaticData(vsd) => {
                println!("MMSI:  {}", vsd.mmsi);
                println!("Flag:  {}", vsd.country().unwrap());
                println!("Name:  {}", vsd.name.unwrap());
                println!("Type:  {}", vsd.ship_type);
                println!("");
            }
            ParsedMessage::Gga(gga) => {
                println!("Source:    {}", gga.source);
                println!("Latitude:  {:.6}°", gga.latitude.unwrap());
                println!("Longitude: {:.6}°", gga.longitude.unwrap());
                println!("");
            }
            ParsedMessage::Rmc(rmc) => {
                println!("Source:  {}", rmc.source);
                println!("Speed:   {:.1} kts", rmc.sog_knots.unwrap());
                println!("Bearing: {}°", rmc.bearing.unwrap());
                println!("Time:    {}", rmc.timestamp.unwrap());
                println!("");
            }
            _ => {}
        }
    }
}
